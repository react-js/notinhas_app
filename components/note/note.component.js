import React from "react";
import {FlatList, ActivityIndicator, Text, View, SafeAreaView, StyleSheet} from "react-native";

export default class Note extends React.Component <{}> {
    constructor(props) {
        super(props);
        this.state = { 
            isLoading: true,
            dataSource: []
        };
    }

    componentDidUpdate() {
        console.log(this.state);
    }
    componentDidMount() {
        return fetch("http://10.0.0.122:9000/notes/")
            .then((response) => response.json())
            .then((json) => {
                this.setState({
                    isLoading: false, 
                    dataSource: json.notes
                }, function (){

                });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    render() {
        if(this.state.isLoading){
            return (
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }

        return (
            <SafeAreaView >
                <FlatList
                    data={this.state.dataSource}
                    renderItem={({ item }) => (
                        <View style={styles.item}>
                            <Text style={styles.title}>{item.title}:{item.created}</Text>
                            <Text style={styles.body}>{item.body}</Text>
                        </View>
                        
                    )}
                    keyExtractor={(item, index) => item._id}>
                </FlatList>
            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    item: {
        alignItems: "center", 
        backgroundColor: "#abcdef",
        flexGrow: 1, 
        margin: 4, 
        padding: 20
    },
    title: {
        color: "#000",
        fontSize: 22,
    },
    body: {
        color: "#82807D",
        fontSize: 18
    }
});